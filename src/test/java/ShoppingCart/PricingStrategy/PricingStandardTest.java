package ShoppingCart.PricingStrategy;

import ShoppingCart.Product.Apple;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

class PricingStandardTest {

    @Test
    void computePrice() {
        PricingStandard pricingStandard = new PricingStandard();
        assertEquals(BigDecimal.valueOf(0.8), pricingStandard.computePrice(Apple.getInstance().getPrice(), 4));
    }
}