package ShoppingCart.PricingStrategy;

import ShoppingCart.Product.Apple;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

class PricingDiscountThreeForTwoTest {

    @Test
    void computePrice() {
        PricingDiscountThreeForTwo pricingDiscountThreeForTwo = new PricingDiscountThreeForTwo();
        assertEquals(BigDecimal.valueOf(0.6), pricingDiscountThreeForTwo.computePrice(Apple.getInstance().getPrice(), 4));
    }
}