package ShoppingCart.PricingStrategy;

import ShoppingCart.Product.Apple;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

class PricingDiscountOneForOneFreeTest {

    @Test
    void computePrice() {
        PricingDiscountOneForOneFree pricingDiscountOneForOneFree = new PricingDiscountOneForOneFree();
        assertEquals(BigDecimal.valueOf(0.4), pricingDiscountOneForOneFree.computePrice(Apple.getInstance().getPrice(), 4));
    }
}