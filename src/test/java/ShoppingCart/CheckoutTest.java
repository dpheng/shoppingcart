package ShoppingCart;

import ShoppingCart.Product.Apple;
import ShoppingCart.Product.Orange;
import ShoppingCart.Product.Watermelon;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

class CheckoutTest {
    Customer customer = new Customer();
    Checkout checkout = new Checkout();

    @Test
    void checkout() {
        customer.add(Apple.getInstance(), 4);
        customer.add(Orange.getInstance(), 3);
        customer.add(Watermelon.getInstance(), 5);
        checkout.checkout(customer.getShoppingCart());
        assertEquals(BigDecimal.valueOf(5.1), checkout.getTotalPrice());
    }

    @Test
    void checkoutAfterCheckoutPrintBill() {
        customer.add(Apple.getInstance(), 4);
        customer.add(Orange.getInstance(), 3);
        customer.add(Watermelon.getInstance(), 5);
        checkout.checkout(customer.getShoppingCart());
        checkout.printBill(customer.getShoppingCart());
        checkout.checkout(customer.getShoppingCart());
        assertEquals(BigDecimal.valueOf(0), checkout.getTotalPrice());
    }

    @Test
    void pricingWithoutDiscountOnWatermelonThreeForTwo() {
        customer.add(Watermelon.getInstance(), 2);
        checkout.checkout(customer.getShoppingCart());
        assertEquals(BigDecimal.valueOf(1.6), checkout.getTotalPrice());
    }

    @Test
    void pricingWithoutDiscountOnAppleOneGetOneFree() {
        customer.add(Apple.getInstance(), 1);
        checkout.checkout(customer.getShoppingCart());
        assertEquals(BigDecimal.valueOf(0.2), checkout.getTotalPrice());
    }
}