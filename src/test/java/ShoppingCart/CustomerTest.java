package ShoppingCart;

import ShoppingCart.Product.Apple;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CustomerTest {
    Customer customer = new Customer();

    @Test
    void countZeroProduct() {
        assertEquals(null, customer.getShoppingCart().get(Apple.getInstance()));
    }

    @Test
    void addTenProduct() {
        customer.add(Apple.getInstance(), 10);
        assertEquals(10, customer.getShoppingCart().get(Apple.getInstance()));
    }

    @Test
    void addZeroProduct() {
        customer.add(Apple.getInstance(), 0);
        assertEquals(null, customer.getShoppingCart().get(Apple.getInstance()));
    }

    @Test
    void removeTenProduct() {
        customer.add(Apple.getInstance(), 10);
        customer.remove(Apple.getInstance(), 10);
        assertEquals(null, customer.getShoppingCart().get(Apple.getInstance()));
    }

    @Test
    void removeNineProduct() {
        customer.add(Apple.getInstance(), 10);
        customer.remove(Apple.getInstance(), 9);
        assertEquals(1, customer.getShoppingCart().get(Apple.getInstance()));
    }

    @Test
    void removeZeroProduct() {
        customer.add(Apple.getInstance(), 10);
        customer.remove(Apple.getInstance(), 0);
        assertEquals(10, customer.getShoppingCart().get(Apple.getInstance()));
    }
}