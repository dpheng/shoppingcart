package ShoppingCart.Product;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class OrangeTest {
    Orange orange = Orange.getInstance();
    @Test
    void defaultPrice() {
        assertEquals(0.5, orange.getPrice());
    }


    @Test
    void defaultName() {
        assertEquals("Orange", orange.getName());
    }
}