package ShoppingCart.Product;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AppleTest {
    Apple apple = Apple.getInstance();

    @Test
    void defaultPrice() {
        assertEquals(0.2, apple.getPrice());
    }


    @Test
    void defaultName() {
        assertEquals("Apple", apple.getName());
    }
}