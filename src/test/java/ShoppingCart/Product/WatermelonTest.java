package ShoppingCart.Product;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WatermelonTest {
    Watermelon watermelon = Watermelon.getInstance();

    @Test
    void defaultPrice() {
        assertEquals(0.8, watermelon.getPrice());
    }


    @Test
    void defaultName() {
        assertEquals("Watermelon", watermelon.getName());
    }
}