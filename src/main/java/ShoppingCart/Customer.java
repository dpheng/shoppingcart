package ShoppingCart;

import ShoppingCart.Product.Product;

import java.util.*;

public class Customer {
    private Map<Product, Integer> shoppingCart = new HashMap<>();

    public void add(Product product, int quantity) {
        Integer currentQuantity = (shoppingCart.get(product) == null) ? 0 : shoppingCart.get(product);
        Integer newQuantity = currentQuantity + quantity;
        if (newQuantity > 0)
            shoppingCart.put(product, newQuantity);
    }

    public void remove(Product product, int quantity) {
        Integer currentQuantity = (shoppingCart.get(product) == null) ? 0 : shoppingCart.get(product);
        Integer newQuantity = currentQuantity - quantity;

        if (newQuantity <= 0)
            shoppingCart.remove(product);
        else
            shoppingCart.put(product, newQuantity);
    }

    public void displayCart() {
        for (Map.Entry<Product, Integer> product : shoppingCart.entrySet()) {
            System.out.println(product.getKey().getName() + " -> " + product.getValue());
        }
    }

    public Map<Product, Integer> getShoppingCart() {
        return shoppingCart;
    }
}
