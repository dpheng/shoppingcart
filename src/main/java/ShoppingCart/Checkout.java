package ShoppingCart;

import ShoppingCart.PricingStrategy.IPricingStrategy;
import ShoppingCart.PricingStrategy.PricingDiscountOneForOneFree;
import ShoppingCart.PricingStrategy.PricingDiscountThreeForTwo;
import ShoppingCart.PricingStrategy.PricingStandard;
import ShoppingCart.Product.Apple;
import ShoppingCart.Product.Orange;
import ShoppingCart.Product.Product;
import ShoppingCart.Product.Watermelon;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class Checkout {
    private BigDecimal totalPrice = new BigDecimal(0.0);
    Map<Product, IPricingStrategy> productDiscount = new HashMap<>();

    public Checkout() {
        productDiscount.put(Apple.getInstance(), new PricingDiscountOneForOneFree());
        productDiscount.put(Watermelon.getInstance(), new PricingDiscountThreeForTwo());
        productDiscount.put(Orange.getInstance(), new PricingStandard());
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void checkout(Map<Product, Integer> shoppingCart) {
        IPricingStrategy pricingStrategy;

        for (Map.Entry<ShoppingCart.Product.Product, Integer> Product : shoppingCart.entrySet()) {
            Product model = Product.getKey();
            pricingStrategy = productDiscount.get(model);
            totalPrice = totalPrice.add(pricingStrategy.computePrice(model.getPrice(), Product.getValue()));
        }
    }

    public void printBill(Map<Product, Integer> shoppingCart) {
        System.out.println("Total price : " + getTotalPrice());
        shoppingCart.clear();
        totalPrice = new BigDecimal(0.0);
    }
}
