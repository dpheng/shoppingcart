package ShoppingCart.Product;

public class Apple extends Product {
    private static Apple apple = null;

    private Apple() {
        super("Apple", 0.2);
    }

    public static Apple getInstance() {
        if (apple == null)
            apple = new Apple();
        return apple;
    }
}
