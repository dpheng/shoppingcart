package ShoppingCart.Product;

public class Orange extends Product {
    private static Orange orange = null;

    private Orange() {
        super("Orange", 0.5);
    }

    public static Orange getInstance() {
        if (orange == null)
            orange = new Orange();
        return orange;
    }
}
