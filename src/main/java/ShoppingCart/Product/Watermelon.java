package ShoppingCart.Product;

public class Watermelon extends Product {
    private static Watermelon watermelon = null;

    private Watermelon() {
        super("Watermelon", 0.8);
    }

    public static Watermelon getInstance() {
        if (watermelon == null)
            watermelon = new Watermelon();
        return watermelon;
    }
}
