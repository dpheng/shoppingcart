package ShoppingCart.PricingStrategy;

import java.math.BigDecimal;

public interface IPricingStrategy {
    BigDecimal computePrice(double price, int quantity);
}
