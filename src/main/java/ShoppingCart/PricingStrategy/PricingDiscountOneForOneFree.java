package ShoppingCart.PricingStrategy;

import java.math.BigDecimal;

public class PricingDiscountOneForOneFree implements IPricingStrategy {
    @Override
    public BigDecimal computePrice(double price, int quantity) {
        int freeQuantity = (quantity - (quantity % 2)) / 2;
        return (BigDecimal.valueOf(quantity).subtract(BigDecimal.valueOf(freeQuantity))).multiply(BigDecimal.valueOf(price));

    }
}
