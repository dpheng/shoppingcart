package ShoppingCart.PricingStrategy;

import java.math.BigDecimal;

public class PricingDiscountThreeForTwo implements IPricingStrategy {
    @Override
    public BigDecimal computePrice(double price, int quantity) {
        int freeQuantity = (quantity - (quantity % 3)) / 3;
        return (BigDecimal.valueOf(quantity).subtract(BigDecimal.valueOf(freeQuantity))).multiply(BigDecimal.valueOf(price));

    }
}
