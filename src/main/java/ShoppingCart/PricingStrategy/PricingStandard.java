package ShoppingCart.PricingStrategy;

import java.math.BigDecimal;

public class PricingStandard implements IPricingStrategy {
    @Override
    public BigDecimal computePrice(double price, int quantity) {
        return BigDecimal.valueOf(price).multiply(BigDecimal.valueOf(quantity));
    }
}
