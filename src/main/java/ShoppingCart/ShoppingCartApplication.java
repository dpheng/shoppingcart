package ShoppingCart;

import ShoppingCart.Product.Apple;
import ShoppingCart.Product.Orange;
import ShoppingCart.Product.Product;
import ShoppingCart.Product.Watermelon;

import java.util.Scanner;

public class ShoppingCartApplication {
    public static int chooseQuantity() {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("Choose quantity : ");
            String input = scanner.next();
            boolean isNumeric = input.chars().allMatch(o -> Character.isDigit(o));
            if (isNumeric) {
                try {
                     return Integer.parseInt(input);
                } catch (NumberFormatException e) {
                    System.out.println("Invalid input");
                }
            } else {
                System.out.println("Invalid input");
            }
        }
    }


    public static Product chooseArticle() {
        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println("1. Apple");
            System.out.println("2. Orange");
            System.out.println("3. Watermelon");
            String input = scanner.next();
            switch (input) {
                case "1":
                    return Apple.getInstance();
                case "2":
                    return Orange.getInstance();
                case "3":
                    return Watermelon.getInstance();
                default:
                    System.out.println("Wrong input");
                    break;
            }
        }
    }

    public static void main(String[] args) {
        Customer customer = new Customer();
        Checkout checkout = new Checkout();

        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("1. Add article");
            System.out.println("2. Remove article");
            System.out.println("3. Checkout and print bill");
            System.out.println("4. Display cart");
            System.out.println("5. Exit article");
            String input = scanner.next();
            switch (input) {
                case "1" :
                    customer.add(chooseArticle(), chooseQuantity());
                    break;
                case "2" :
                    customer.remove(chooseArticle(), chooseQuantity());
                    break;
                case "3" :
                    checkout.checkout(customer.getShoppingCart());
                    checkout.printBill(customer.getShoppingCart());
                    break;
                case "4" :
                    customer.displayCart();
                    break;
                case "5" :
                    System.exit(0);
                default:
                    System.out.println("Wrong input");
                    break;
            }
        }
    }
}
