# ShoppingCart

ShoppingCart application in command line.
Require JAVA 8 or more.
Require JUnit 5 or more.

Installation:
    
    cd ShoppingCart/
    mvn clean install test
    
Run in console:

    cd ShoppingCart/target/
    java -cp ShoppingCart-1.0-SNAPSHOT.jar ShoppingCart.ShoppingCartApplication
    